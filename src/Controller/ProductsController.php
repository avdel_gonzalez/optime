<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Category;
use App\Entity\Product;
use App\Form\ProductType;

class ProductsController extends AbstractController
{
    /**
     * @Route("/products", name="products")
     */
    public function index(): Response
    {
        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();
        return $this->render('products/index.html.twig', [
            'controller_name' => 'ProductsController',
            'products' => $products
        ]);
    }

    public function create(Request $request)
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $data = $form->getData();
            $product->setCode($data->getCode());
            $product->setName($data->getName());
            $product->setDescription($data->getDescription());
            $product->setBrand($data->getBrand());
            $product->setPrice($data->getPrice());
            $product->setCategory($data->getCategory());
            $product->setCreatedAt(new \DateTime('now'));
            $product->setUpdatedAt(new \DateTime('now'));
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('products');
        }

        return $this->render('products/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function edit(Product $product, Request $request){
        $form = $this->createForm(ProductType::class, $product);
        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository(Product::class)->find($product->getId());
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            $product->setCode($data->getCode());
            $product->setName($data->getName());
            $product->setDescription($data->getDescription());
            $product->setBrand($data->getBrand());
            $product->setPrice($data->getPrice());
            $product->setCategory($data->getCategory());
            $product->setUpdatedAt(new \DateTime('now'));

            $entityManager->flush();

            return $this->redirectToRoute('products');
        }

        return $this->render('products/edit.html.twig', [
            'form' => $form->createView()
        ]);

    }

    public function delete(Product $product){
        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository(Product::class)->find($product->getId());
        $entityManager->remove($product);
        $entityManager->flush();
        return $this->redirectToRoute('products');
    }
}
