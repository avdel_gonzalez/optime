<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Category;
use App\Form\CategoryType;

class CategoriesController extends AbstractController
{
    public function index(): Response
    {
        $categories = $this->getDoctrine()->getRepository(Category::class)->findAll();
        return $this->render('categories/index.html.twig', [
            'controller_name' => 'CategoriesController',
            'categories' => $categories
        ]);
    }

    public function create(Request $request)
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $data = $form->getData();
            $category->setName($data->getName());
            $category->setActive($data->getActive());
            $category->setCreatedAt(new \DateTime('now'));
            $category->setUpdatedAt(new \DateTime('now'));
            $em->persist($category);
            $em->flush();

            return $this->redirectToRoute('categories');
        }

        return $this->render('categories/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    public function edit(Category $category, Request $request){
        $form = $this->createForm(CategoryType::class, $category);
        $entityManager = $this->getDoctrine()->getManager();
        $category = $entityManager->getRepository(Category::class)->find($category->getId());
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $data = $form->getData();
            $category->setName($data->getName());
            $category->setActive($data->getActive());
            $category->setUpdatedAt(new \DateTime('now'));

            $entityManager->flush();

            return $this->redirectToRoute('categories');
        }

        return $this->render('categories/edit.html.twig', [
            'form' => $form->createView()
        ]);

    }

    public function delete(Category $category){
        $entityManager = $this->getDoctrine()->getManager();
        $category = $entityManager->getRepository(Category::class)->find($category->getId());
        if(count($category->getProducts()) == 0){
            $entityManager->remove($category);
            $entityManager->flush();
            return $this->redirectToRoute('categories');
        } else {
            return $this->redirectToRoute('categories');
        }
    }
}
